var tileSet = new Array();
var gameWidth = 768;
var gameHeight = 512;
var tileSize = 64;
var masterSpriteList = new Array();
var solidSpriteList = new Array(); //List used for collisions
var interactList = new Array(); //List used for interactions
var guiList = new Array(); //List used for GUI sprites.

var display =
{
  initialize: function()
  {
    console.log('Initializing game...');
    //Does stuff

    this.setUpRender();
    this.loadPIXI();
    interact.setup();
  },

  setup: function()
  {
    //PIXI stuff goes in here!
    console.log('Running setup...');

    display.makeSprites();
    display.renderer.render(display.stage);

    function gameLoop()
    {
      //runs the game

      //update warlock
      warlock.checkDir(); //Warlock mvmt
      //update interactions
      interact.update();


      //update customers
      //Check timer on new customers
      //customer mvmt
      //Customer status update

      setPositions();
      requestAnimationFrame(gameLoop);
      display.renderer.render(display.stage);

      display.stage.on('mousedown', mouseL.mouseEventListener);
      display.stage.on('rightdown', mouseR.mouseEventListener);
    };

    function setPositions()
    {
      //sets positions
      //loop through the current positions - in pixels - of all movable objects

      for (var i = 0; i < masterSpriteList.length; i++)
      {
        //if sprite is visible, and can move, do this:
        masterSpriteList[i].x += masterSpriteList[i].velX;
        masterSpriteList[i].y += masterSpriteList[i].velY;
        masterSpriteList[i].tileX = masterSpriteList[i].posX / tileSize;
        masterSpriteList[i].tileY = masterSpriteList[i].posY / tileSize;
      }
    };

    gameLoop();
  },

  setUpRender: function()
  {
    this.renderer = PIXI.autoDetectRenderer(
      768, 512,
      {antialias: false, transparent: false, resolution: 1}
    );

    document.body.appendChild(this.renderer.view);

    this.renderer.view.style.border = '2px dashed red';
    this.renderer.backgroundColor = 0x061639;
    this.stage = new PIXI.Container();
    this.stage.interactive = true;
    this.stage.hitArea = new PIXI.Rectangle(0, 0, gameWidth, gameHeight);
    this.renderer.render(this.stage);
  },

  loadPIXI: function()
  {
    PIXI.loader
      .add('pepe', 'images/timicus_sprite.png') //Add all assets here
      .add('wall', 'images/floor2.png')
      .add('wood', 'images/floor1.png')
      .add('table', 'images/table.png')
      .load(this.setup);

  },

  makeSprites: function()
  {
    //Size of all should be 64x64, or tileSizextileSize

    //populate array masterSpriteList[]

    var i = 0; //master sprite counter
    var j = 0; //solid sprite counter
    var k = 0; //interact list counter



    //Populate background - floor, wall. 12 x 8
    for (var x = 0; x < 12; x++) //floor
    {
      for (var y = 0; y < 8; y++)
      {
        masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.wood.texture);
        masterSpriteList[i].width = tileSize;
        masterSpriteList[i].height = tileSize;
        masterSpriteList[i].tileX = x;
        masterSpriteList[i].tileY = y;
        masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
        masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
        masterSpriteList[i].velX = 0;
        masterSpriteList[i].velY = 0;
        this.stage.addChild(masterSpriteList[i]);
        i++;
      }
    }

    //wall
    for (var x = 0; x < 12; x++) //top
    {
      masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.wall.texture);
      masterSpriteList[i].width = tileSize;
      masterSpriteList[i].height = tileSize;
      masterSpriteList[i].tileX = x;
      masterSpriteList[i].tileY = 0;
      masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
      masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
      masterSpriteList[i].velX = 0;
      masterSpriteList[i].velY = 0;
      masterSpriteList[i].isSolid = true;
      this.stage.addChild(masterSpriteList[i]);
      solidSpriteList[j] = masterSpriteList[i];
      i++;
      j++;
    }

    for (var x = 0; x < 12; x++) //bottom
    {
      masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.wall.texture);
      masterSpriteList[i].width = tileSize;
      masterSpriteList[i].height = tileSize;
      masterSpriteList[i].tileX = x;
      masterSpriteList[i].tileY = 7;
      masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
      masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
      masterSpriteList[i].velX = 0;
      masterSpriteList[i].velY = 0;
      masterSpriteList[i].isSolid = true;
      this.stage.addChild(masterSpriteList[i]);
      solidSpriteList[j] = masterSpriteList[i];
      i++;
      j++;
    }

    for (var y = 1; y < 7; y++) //left
    {
      masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.wall.texture);
      masterSpriteList[i].width = tileSize;
      masterSpriteList[i].height = tileSize;
      masterSpriteList[i].tileX = 0;
      masterSpriteList[i].tileY = y;
      masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
      masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
      masterSpriteList[i].velX = 0;
      masterSpriteList[i].velY = 0;
      masterSpriteList[i].isSolid = true;
      this.stage.addChild(masterSpriteList[i]);
      solidSpriteList[j] = masterSpriteList[i];
      i++;
      j++;
    }

    for (var y = 1; y < 7; y++) //right
    {
      masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.wall.texture);
      masterSpriteList[i].width = tileSize;
      masterSpriteList[i].height = tileSize;
      masterSpriteList[i].tileX = 11;
      masterSpriteList[i].tileY = y;
      masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
      masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
      masterSpriteList[i].velX = 0;
      masterSpriteList[i].velY = 0;
      masterSpriteList[i].isSolid = true;
      this.stage.addChild(masterSpriteList[i]);
      solidSpriteList[j] = masterSpriteList[i];
      i++;
      j++;
    }



    //Populate foreground - tables, chairs

    //table
    masterSpriteList[i] = new PIXI.Sprite(PIXI.loader.resources.table.texture);
    masterSpriteList[i].width = tileSize;
    masterSpriteList[i].height = tileSize;
    masterSpriteList[i].tileX = 5;
    masterSpriteList[i].tileY = 4;
    masterSpriteList[i].x = masterSpriteList[i].tileX * tileSize;
    masterSpriteList[i].y = masterSpriteList[i].tileY * tileSize;
    masterSpriteList[i].velX = 0;
    masterSpriteList[i].velY = 0;
    masterSpriteList[i].isSolid = true;
    this.stage.addChild(masterSpriteList[i]);
    solidSpriteList[j] = masterSpriteList[i];
    interactList[k] =
    {
      x: masterSpriteList[i].tileX,
      y: masterSpriteList[i].tileY,
      id: 'table'
    };
    i++;
    j++;
    k++;


    //Populate characters - warlock, customers
    warlock.setup(i, j, k);
    i++;
    j++;
    k++;

    //UI elements

  },

};
