//This will also do stuff
var menuIsOn = false;

var mouseR =
{
  mouseEventListener: function(mouseEvent)
  {
    mouseR.x = mouseEvent.data.originalEvent.clientX; //Gets position of mouse on screen
    mouseR.y = mouseEvent.data.originalEvent.clientY;

    this.tileX = Math.floor(mouseR.x / tileSize); //Converts to tile
    this.tileY = Math.floor(mouseR.y / tileSize);

    interact.go(this.tileX, this.tileY); //Returns tile
  }
};

var interact =
{
  /* 2D array:
    var items = [
    [1, 2],
    [3, 4],
    [5, 6]
    ];
*/
  interactions: Array(),
  tX: 0,
  tY: 0,
  currentInteractions: Array(),
  setup: function()
  {
    //initializes where you can interact in a 2D array
    //I have no idea if this will actually work!
    this.interactions = new Array(20);
    for (i = 0; i < 20; i++)
    {
      /*
      Expanded using method shown here:
      http://www.stephanimoroni.com/how-to-create-a-2d-array-in-javascript/
      */
      var second = [];
      for (j = 0; j < 20; j++)
      {
        second[j] = false;
      }
      this.interactions[i] = second;
    }

    for (i = 0; i < interactList.length; i++)
    {
      //Populate columns of interactions grid
      this.interactions[interactList[i].x][interactList[i].y] = interactList[i];
    }
  },
  update: function()
  {
    while (this.interactions.length > 0)
    {
        this.interactions.pop();
    }
    this.setup();
  },
  go: function(tX, tY)
  {
    //sends interaction to do whatever
    if (!menuIsOn)
    {
      menuIsOn = true;
      var i = 0; //i is the current number of interactions
      this.tX = tX;
      this.tY = tY;
      console.log(this.interactions[tX][tY]);
      if (this.interactions[tX][tY])
      {
        if (this.interactions[tX][tY].id == 'customer')
        {
        }
        if (this.interactions[tX][tY].id == 'table')
        {
          console.log('table menu called');
          this.currentInteractions[i] == 'move'; //Add movement to table
          i++;
        }
        //etc.
      }
      else
      {
        this.currentInteractions[i] = 'move';
        i++;
      }
    }
  },
  showMenu: function()
  {
    //once all the options are populated, show the menu
  },
  menuClick: function()
  {
    menuIsOn = false;
    //check menu option clicked
    var optionClicked = 0;


    //execute option
    console.log('menuClick called');
    if (this.currentInteractions[optionClicked] == 'move')
    {
      warlock.moveTo(this.tX, this.tY);
    }
  }
};
