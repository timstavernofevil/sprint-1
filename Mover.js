//Version 0.11
function pathfinder(personX, personY, targetX, targetY)
{

  //A list of all passable nodes that have been scanned
  var openList[];
  //A list of passable nodes that have been used to scan from
  var closedList[];

  //node constructor
  function node(tileX, tileY)
  {
    var ptr = null;
    var x = tileX;
    var y = tileY;
    var g = 1000;
    var h = 1000;
    var f = 2000;
    var openListIndex = -1;
    var closedListIndex = -1;
  }

  //Initializes start and target nodes
  var startNode = new node(personX, personY);
  var targetNode = new node(targetX, targetY);
  startNode.g = 0;
  startNode.h = Math.abs(targetNode.x - startNode.x) + Math.abs(targetNode.y - startNode.y);
  startNode.f = startNode.h + startNode.g;
  startNode.openListIndex = 0;
  startNode.closedListIndex = 0;
  targetNode.h = 0;

  openList.push(startNode);
  closedList.push(startNode);

  //Analyzes nodes
  function checkNode(node)
  {
    var alreadyInOpen = false;
    node.openListIndex = -1;

    var tempG = currentNode.g + 1;
    var tempH = Math.abs(targetNode.x - node.x) + Math.abs(targetNode.y - node.y);

    //Checks if node is already in open list
    for (var i = 0; i < openList.length; i++)
    {
      if (node.x == openList[i].x && node.y == openList[i].y)
      {
        alreadyInOpen = true;
        node.g = openList[i].g, node.h = openList[i].h, node.f = openList[i].f;
        node.openListIndex = i;
      }
    }
    //If a solid object, ie not passable, breaks - thus not adding to open list
    if (alreadyInOpen == false) {
      for (var i = 0; i < solidSpriteList.size; i++) {
        if (solidSpriteList[i].tileX == node.x && solidSpriteList[i].tileY == node.y) {
          break;
        }
      }
    }

    //If analyzed for the first time, give it these values
    if (alreadyInOpen == false)
    {
      node.g = tempG;
      node.h = tempH;
      node.f = tempG + tempH;
      node.ptr = openListIndex[currentNode.openListIndex];
      openList.push(node);
      node.openListIndex = openList.length - 1;
    }

    //If being reanalyzed, update values only if there is a better way to reac
    if (alreadyInOpen == true)
    {
      if (node.f > tempH + tempG)
      {
        node.g = tempG;
        node.h = tempH;
        node.f = tempG + tempH;
        node.ptr = openList[currentNode.openListIndex];
        openList[node.openListIndex].g = node.g;
        openList[node.openListIndex].h = node.h;
        openList[node.openListIndex].f = node.f;
        openList[node.openListIndex].ptr = node.ptr;
        for (var i = 0; i < closedList.size; i++)
        {
          if (node.x == closedList[i].x && node.y == closedList[i].y)
          {
            closedList[i].g = node.g;
            closedList[i].h = node.h;
            closedList[i].f = node.f;
            closedList[i].ptr = node.ptr;
          }
        }
      }
    }
  }

  //Four vars reffering to nodes around the target node
  var up;
  var down;
  var left;
  var right;

//While the target node is not in openList
  while (targetNode.openListIndex < 0)
  {
    up = new Node(currentNode.x, currentNode.y - 1);
    down = new Node(currentNode.x, currentNode.y + 1);
    left = new Node(currentNode.x - 1, currentNode.y);
    right = new Node(currentNode.x + 1, currentNode.y);

    //If destination node is not in open list, analyze each node
    if (openList[openList.length - 1].h != 0)
      checkNode(up);
    if (openList[openList.length - 1].h != 0)
      checkNode(down);
    if (openList[openList.length - 1].h != 0)
      checkNode(left);
    if (openList[openList.length - 1].h != 0)
      checkNode(right);

    //figure out the next node to analyze
    var nextCurrentNode;
    while (nextCurrentNode == null)
    {
      int lowestF = 1000;
      int lowestFIndex = -1;
      for (var i = 0; i < openList.size; i++)
      {
        if (openList[i] < lowestF && openList[i].closedListIndex < 0)
        {
          lowestF = openList[i].f;
          lowestFIndex = i;
        }
      }
      //update the closed list
      nextCurrentNode = openList[lowestFIndex];
      currentNode = nextCurrentNode;
      currentNode.closedListIndex = closedListIndex.length;
      openList[currentNode.openListIndex].closedListIndex = currentNode.closedListIndex;
      closedList.push(currentNode);
    }
  }

  //Follow the node ptr to create an array of the node path
  var reverseMoveList[];
  currentNode = openList[openList.length - 1];
  while (currentNode.ptr != null)
  {
    reverseMoveList.push(currentNode);
    currentNode = currentNode.ptr;
  }

  //Put this in first to last order, pulling out the x and y coordinates
  var moveList[];
  for (i = 0; i < reverseMoveList.length - 1; i++)
  {
    moveList.push({x: reverseMoveList[reverseMoveList.length - 1 - i].x, y: reverseMoveList[reverseMoveList.length - 1 - i].y});
  }
  return moveList;
}