var warlock =
{
  wtx: 2,
  wty: 4,
  mslPos: 0,
  sslPos: 0,
  islPos: 0,
  moving: false,
  movePos: 0,
  setup: function(msl, ssl, isl)
  {
    console.log('Setting up warlock!');
    this.mslPos = msl; //Position in master list
    this.sslPos = ssl; //Position in solid list
    this.islPos = isl; //Position in interact list
    this.moving = false;
    this.move = new Array();
    this.movePos = 0;

    //Basic sprite setup
    this.sprite = new PIXI.Sprite(PIXI.loader.resources.pepe.texture);
    this.sprite.width = tileSize;
    this.sprite.height = tileSize;
    this.sprite.tileX = this.wtx;
    this.sprite.tileY = this.wty;
    this.sprite.x = this.sprite.tileX * tileSize;
    this.sprite.y = this.sprite.tileY * tileSize;
    this.sprite.velX = 0;
    this.sprite.velY = 0;
    this.sprite.isSolid = false;
    //Adding refs
    display.stage.addChild(this.sprite);
    masterSpriteList[this.mslPos] = this.sprite;
    solidSpriteList[this.sslPos] = this.sprite;
    interactList[this.islPos] =
    {
      x: this.sprite.tileX,
      y: this.sprite.tileY,
      id: 'warlock'
    };

  },


  checkDir: function()
  {
    //This should be checked every gameLoop.
    if (this.moving)
    {
      //move

      this.wtx = this.sprite.x / tileSize;

      if (this.move[this.movePos].x > this.wtx)
      {
        this.movement('right');
      }
      if (this.move[this.movePos].x < this.wtx)
      {
        this.movement('left');
      }
      if (this.move[this.movePos].y > this.wty)
      {
        this.movement('down');
        this.wty = Math.floor(this.sprite.y / tileSize);
      }
      if (this.move[this.movePos].y < this.wty)
      {
        this.movement('up');
        this.wty = Math.ceil(this.sprite.y / tileSize);
      }

      //check if done with current move
      if (this.wtx == this.move[this.movePos].x && this.wty == this.move[this.movePos].y)
      {
        this.movePos++;
      }
    }
    //check if done moving
    if (this.movPos >= this.move.length || (!this.move[this.movePos]))
    {
      this.movPos = 0;

      while (this.move.length > 0)
      {
          this.move.pop();
      }
      this.moving = false;
      this.sprite.velX = 0;
      this.sprite.velY = 0;
      this.sprite.x = this.wtx * tileSize;
      this.sprite.y = this.wty * tileSize;
      this.sprite.tileX = this.wtx;
      this.sprite.tileY = this.wty;
    }

  },

  moveTo: function(dtX, dtY)
  {
    //Call the pather using current tiles and destination tiles
    //wtx, wty - current x,y
    //dtx, dty - destination x,y


    if (!this.moving)
    {
      this.moving = true;

      //get movement list
      //this.move = pathfinder(this.wtx, this.wty, dtX, dtY);

      //dummy movement list
      this.move[0] = {x: this.wtx, y: (this.wty - 1)};
      this.move[1] = {x: (this.wtx + 1), y: (this.wty - 1)};
      this.move[2] = {x: (this.wtx), y: (this.wty - 1)};
      this.move[3] = {x: (this.wtx), y: (this.wty)};

      this.movePos = 0;
    }

  },

  movement: function(direction)
  {

    if (direction == 'up')
    {
      //go up
      this.sprite.velX = 0;
      this.sprite.velY = -2;
    }
    if (direction == 'down')
    {
      //go down
      this.sprite.velX = 0;
      this.sprite.velY = 2;
    }
    if (direction == 'left')
    {
      //go left
      this.sprite.velX = -2;
      this.sprite.velY = 0;
    }
    if (direction == 'right')
    {
      //go right
      this.sprite.velX = 2;
      this.sprite.velY = 0;
    }
  }
};
